const routerConfig = [
  {
    path: '/',
    component: '@/layouts/index',
    routes: [
      {
        exact: true,
        path: '/Demo1',
        name: 'Demo1',
        component: '@/pages/Demo1/index',
        icon: 'SettingOutlined',
      },
      {
        exact: true,
        path: '/Demo2',
        name: 'Demo2',
        component: '@/pages/Demo2/index',
        icon: 'AppstoreOutlined',
      },
    ],
  },
];
export default routerConfig;
