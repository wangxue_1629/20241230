import { defineConfig } from 'umi';
import routerConfig from './config/router.config';

export default defineConfig({
  nodeModulesTransform: {
    type: 'none',
  },
  routes: routerConfig,
  dva: {
    immer: true,
    hmr: false,
  },
  ignoreMomentLocale: true,
  hash: true,
  targets: {
    ie: 9,
  },
  fastRefresh: {},
});
