import React, { Fragment } from 'react';

class Demo1 extends React.PureComponent {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <Fragment>
        <div>第一个页面</div>
      </Fragment>
    );
  }
}

export default Demo1;
