import React, { Fragment } from 'react';

class Demo2 extends React.PureComponent {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <Fragment>
        <div>第二个页面</div>
      </Fragment>
    );
  }
}

export default Demo2;
